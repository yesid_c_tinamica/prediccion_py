import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import cv2
import StartIR as ini
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
# print(plt.__file__)
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from io import StringIO
import argparse


def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.compat.v1.Session() as sess:        
      # Get handles to input and output tensors
      ops = tf.compat.v1.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}  
      print(all_tensor_names)    
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.uint8)      
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict


def predecir():        
    for image_path in TEST_IMAGE_PATHS:
        image = Image.open(image_path)
        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np = ini.load_image_into_numpy_array(image)
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        # Actual detection.
        output_dict = run_inference_for_single_image(image_np, ini.detection_graph)                                   
        # Visualization of the results of a detection.
        imageBo = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            CATEGORIY_INDEX,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3, skip_scores = True, max_boxes_to_draw = None)        
        ruta = os.path.join(args.outdir, os.path.basename(image_path))
        cv2.imwrite(ruta, imageBo.astype(np.uint8))  
        azul =  Image.open(ruta)
        azul = ini.load_image_into_numpy_array(azul)
        cv2.imwrite(ruta,azul)              


def rescribir_imagen(directory):
    for img in os.listdir(directory):
        ajusteResize(img,directory)

def rescale_images(directory):    
    for img in os.listdir(directory):
        resize(img, directory, 832, 832)            
        ajusteResize(img,directory)
        ajusteResize(img,directory)            

def ajusteResize(image_path, out_path):
    image = cv2.imread(os.path.join(out_path, image_path))
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    image_pil = Image.fromarray(image).convert("RGB")
    result = np.array(image_pil.convert('RGB'))
    Image.fromarray(result).save(os.path.join(out_path, image_path),quality=100,optimize=True)        
    return result

def resize(image_path, out_path, width, height):    
    image = cv2.imread(os.path.join(out_path, image_path))
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    image_pil = Image.fromarray(image).convert("RGB")

    ratio_w = width / image_pil.width
    ratio_h = height / image_pil.height
    if ratio_w < ratio_h:
        # It must be fixed by width
        resize_width = width
        resize_height = round(ratio_w * image_pil.height)
    else:
        # Fixed by height
        resize_width = round(ratio_h * image_pil.width)
        resize_height = height

    image_resize = image_pil.resize((resize_width, resize_height), Image.ANTIALIAS)
    background = Image.new('RGBA', (width, height), (255, 255, 255, 255))
    background.paste(image_resize, (0, 0))
    result = np.array(background.convert('RGB'))    
    Image.fromarray(result).save(os.path.join(out_path, image_path),quality=100,optimize=True)        
    return result


PATH_TO_TEST_IMAGES_DIR = 'images/test/'
TEST_IMAGE_PATHS = []
IMAGE_SIZE = (8, 8)
CATEGORIY_INDEX = {}

def cargarRutaImagenes(path):    
    if path == None:
        path = PATH_TO_TEST_IMAGES_DIR
    for root, subdirs, files in os.walk(path):
        for file in files:
            if os.path.splitext(file)[1].lower() in ('.jpg', '.jpeg'):
                TEST_IMAGE_PATHS.append(os.path.join(root, file))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Ruta images para cargar en memoria por defecto images/test")
    parser.add_argument('-d', '--directory', type=str, required=False, help='Directory containing the images', default='images\\val')
    parser.add_argument('--outdir', help='Path to output image directory', default='C:\\ProcesadasYolo')
    parser.add_argument('--label', help='Path to config file.', default='clases\\labelmap.pbtx')
    parser.add_argument('--modelo', help='Path to pb file.', default='modelos\\frozen_inference_graph.pb')
    parser.add_argument('--resize', help='1 para hacer resize 0 para no hacer resize', default='0')
    args = parser.parse_args()
    # if (args.resize == '1'):
    #     rescale_images(args.directory)
    # else:
    #     rescribir_imagen(args.directory)
    CATEGORIY_INDEX = ini.cargarModeloPB(args.modelo,args.label)    
    TEST_IMAGE_PATHS = []
    cargarRutaImagenes(args.directory)     
    predecir()    
  